docbook-examples
================

# Introduction

This repository provides examples of building documentation from DocBook
sources.

# Maven

## `package`

To clean and rebuild the documentation, execute the command below.  Files will
be built under directory `target/docbkx`.  Files are copied to directory
`docs`.  If GitHub is being used, then a repository can be configured to serve
static web pages from the `docs` directory.

```bash
mvn \
    clean \
    package
```

# Bash

This section is only of interest if you intend to build documentation using
shell scripts.

It is in no way necessary if you intend to build documentation using the Maven
`docbkx` plugin.

## Prerequisites

### `xsltproc`

Note that the `xmllint` program for validating XML against a DTD is typically
packaged with `xsltproc`.

Mac OS
------

`xsltproc` and `xmllint` are available by default under directory `/usr/bin`.

### `docbook-xsl`

Mac OS
------

`docbook-xsl` can be installed with the command below.

```bash
brew install docbook-xsl
```

The default behavior of the `libxml2` library is to begin searching for
catalogs under `/etc/xml/catalog`.  By setting environment variable
`XML_CATALOG_FILES` to a given directory, you can force the library to begin
searching for catalogs at that location.  Below is code for setting the
variable in `~/.bash_profile` to something sane for `brew`.

```bash
XML_CATALOG_FILES='/usr/local/etc/xml/catalog'
export XML_CATALOG_FILES
```

### `tidy`

Mac OS
------

`tidy` can be installed with the command below.

```bash
brew install tidy-html5
```

### Find Your XML Catalogs

The root XML catalog on your machine could be under `/etc/xml/catalog` or
`/usr/local/etc/xml/catalog` or somewhere similar.

Something like the command below could be used to find where the DocBook 5.0
catalog resides on your machine.

```bash
grep \
    'docbook/5.0/docbook/xml/5.0/catalog.xml' \
    /usr/local/etc/xml/catalog
```

```text
  <nextCatalog catalog="file:///usr/local/Cellar/docbook/5.0/docbook/xml/5.0/catalog.xml"/>
```

Something like the command below could be used to see the `publicId`
corresponding to the DocBook 5.0 DTD.

```bash
grep '<public' /usr/local/Cellar/docbook/5.0/docbook/xml/5.0/catalog.xml
```

```text
<public publicId="-//OASIS//DTD DocBook XML 5.0//EN" uri="dtd/docbook.dtd"/>
```

Something like the command below could be used to see the location of the
`docbook-xsl` catalogs.

```bash
grep docbook-xsl /usr/local/etc/xml/catalog
```

```text
  <nextCatalog catalog="file:///usr/local/Cellar/docbook-xsl/1.79.1/docbook-xsl/catalog.xml"/>
  <nextCatalog catalog="file:///usr/local/Cellar/docbook-xsl/1.79.1/docbook-xsl-ns/catalog.xml"/>
```

In this case, various XSL files can be found under various directories under
`/usr/local/Cellar/docbook-xsl/1.79.1/docbook-xsl` and
`/usr/local/Cellar/docbook-xsl/1.79.1/docbook-xsl-ns`.

## `build.sh`

The `build.sh` script at `src/main/bin/build.sh` illustrates the use of the
`xmllint` command to validate XML and the `xsltproc` command to build
documentation from DocBook files.

Note that it primarily makes use of XML files under `src/main/doc` and XSLT
files under `src/main/xslt/bash`.

Note that it builds documentation under `target/docs_bash`.

## Using `tidy` to Format HTML

The `tidy` command can be used to make it easier to read HTML.

To format file FILE.html, execute the command below.

```bash
tidy \
    --indent auto \
    --indent-spaces 4 \
    FILE.html
```

# Resources

- [DocBook 5: The Definitive Guide](https://tdg.docbook.org/tdg/5.0/docbook.html)

- [DocBook XSL: The Complete Guide](http://www.sagehill.net/docbookxsl)

- [DocBook XSL Stylesheets: Reference Documentation](http://docbook.sourceforge.net/release/xsl/current/doc)

- [The DoCookBook: Recipes for DocBook Developers](http://doccookbook.sourceforge.net/html/en/DoCookBook.html)

- [docbkx-maven-plugin](http://docbkx-tools.sourceforge.net)

- [docbkx-maven-plugin FAQ](https://github.com/mimil/docbkx-tools/wiki/FAQ)

- [The XSLT C library for GNOME: libxslt](http://xmlsoft.org/XSLT)
