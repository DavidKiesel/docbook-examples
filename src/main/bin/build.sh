#!/bin/bash

##############################################################################
# exit on error

set -e

##############################################################################
# variables

BIN_DIR="$(dirname "$0")"
TOP_DIR="${BIN_DIR}/../../.."
SRC_DIR="${TOP_DIR}/src/main"
CSS_DIR="${SRC_DIR}/css"
XSLT_DIR="${SRC_DIR}/xslt/bash"
BUILD_DIR="${TOP_DIR}/target/docs_bash"

##############################################################################
# clean

rm \
    -rf \
    "${BUILD_DIR}"

mkdir \
    -p \
    "${BUILD_DIR}"

##############################################################################
# css

cp \
    -a \
    "${CSS_DIR}" \
    "${BUILD_DIR}"

##############################################################################
# build documents

PARAMETERS_LIST=(
"numbers_guide-book|http://docbook.sourceforge.net/release/xsl/current/html/docbook.xsl|numbers_guide-book"
"numbers_guide-book|${XSLT_DIR}/docbook-modified.xsl|numbers_guide-book-modified"
"numbers_guide-article-singlefile|http://docbook.sourceforge.net/release/xsl/current/html/docbook.xsl|numbers_guide-article-singlefile"
"numbers_guide-article-singlefile|${XSLT_DIR}/docbook-article-modified.xsl|numbers_guide-article-singlefile-modified"
"numbers_guide-article-singlefile|http://docbook.sourceforge.net/release/xsl/current/html/chunkfast.xsl|numbers_guide-article-singlefile-chunk"
"numbers_guide-article-singlefile|${XSLT_DIR}/chunkfast-article-modified.xsl|numbers_guide-article-singlefile-chunk-modified"
"numbers_guide-article-multifile|${XSLT_DIR}/chunkfast-article-modified.xsl|numbers_guide-article-multifile-chunk-modified"
)

for PARAMETERS in ${PARAMETERS_LIST[@]}
do
    IFS='|' read BASE_DOCUMENT_DIR XSLT BASE_PUBLISH_DIR < <(echo "${PARAMETERS}")

    DOCUMENT_DIR="${SRC_DIR}/doc/${BASE_DOCUMENT_DIR}"
    PUBLISH_DIR="${BUILD_DIR}/${BASE_PUBLISH_DIR}"

    mkdir \
        -p \
        "${PUBLISH_DIR}"

    xmllint \
        --valid \
        --noout \
        "${DOCUMENT_DIR}/document.xml"

    xsltproc \
        --output "${PUBLISH_DIR}/index.html" \
        "${XSLT}" \
        "${DOCUMENT_DIR}/document.xml"

    if [ ! -f  "${PUBLISH_DIR}/index.html" ]
    then
        cp \
            -a \
            "${DOCUMENT_DIR}/frame.html" \
            "${PUBLISH_DIR}/index.html"
    fi
done

##############################################################################
# exit

exit 0
