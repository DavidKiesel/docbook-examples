<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet  
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
>
    <!-- imports ********************************************************* -->
    <xsl:import
        href="http://docbook.sourceforge.net/release/xsl/current/html/docbook.xsl"
    />

    <!-- params ********************************************************** -->
    <!-- http://docbook.sourceforge.net/release/xsl/current/doc/html/ -->

    <!-- set css stylesheet -->
    <xsl:param
        name="html.stylesheet"
        select="'../css/book.css'"
    />

    <!-- default is for book and chapters to generate toc -->
    <xsl:param
        name="generate.toc"
    >
        article    toc
    </xsl:param>

    <!-- default is 2  -->
    <xsl:param
        name="toc.section.depth"
    >
        6
    </xsl:param>

    <!-- set css stylesheet -->
    <xsl:param
        name="section.autolabel"
        select="1"
    />

    <!-- modifications *************************************************** -->

    <xsl:output
        method="html"
        encoding="UTF-8"
        indent="no"
    />
</xsl:stylesheet>
