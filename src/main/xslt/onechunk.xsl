<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet  
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
>
    <!-- imports ********************************************************* -->
    <xsl:import
        href="urn:docbkx:stylesheet/onechunk.xsl"
    />

    <!-- params ********************************************************** -->
    <!-- http://docbook.sourceforge.net/release/xsl/current/doc/html/ -->

    <!-- set output filename; foo will become foo.html -->
    <xsl:param
        name="root.filename"
    >index</xsl:param>
</xsl:stylesheet>
